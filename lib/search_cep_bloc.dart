import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class SearchCepState {}

class SearchCepInitState implements SearchCepState {}

class SearchCepLoading implements SearchCepState {}

class SearchCepError implements SearchCepState {
  final String message;
  SearchCepError(this.message);
}

class SearchCepSuccess implements SearchCepState {
  final Map<dynamic, dynamic> json;
  SearchCepSuccess(this.json);
}

class SearchCepBlc extends Bloc<String, SearchCepState> {
  SearchCepBlc() : super(SearchCepInitState()) {
    on<String>(_searchCep);
  }

  _searchCep(String cep, Emitter<SearchCepState> emit) async {
    try {
      emit(SearchCepLoading());

      final jsonResponse =
          await Dio().get('https://viacep.com.br/ws/$cep/json');
      print('Result data: ${jsonResponse.data}');
      emit(SearchCepSuccess(jsonResponse.data));
    } on DioError {
      emit(SearchCepError('Erro na pesquisa.'));
    }
  }
}
