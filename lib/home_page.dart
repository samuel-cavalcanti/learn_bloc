import 'package:flutter/material.dart';
import 'package:learn_bloc/search_cep_bloc.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _textController = TextEditingController();
  final _searchCepBlc = SearchCepBlc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              TextField(
                controller: _textController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Cep',
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton(
                onPressed: _searchCepA,
                child: const Text('Pesquisar'),
              ),
              const SizedBox(
                height: 10,
              ),
              StreamBuilder<SearchCepState>(
                stream: _searchCepBlc.stream,
                builder: (BuildContext context, snapshot) {
                  if (!snapshot.hasData) return Container();

                  final state = snapshot.data!;

                  if (state is SearchCepInitState) return Container();

                  if (state is SearchCepError) {
                    return Text(
                      state.message,
                      style: const TextStyle(color: Colors.red),
                    );
                  }

                  if (state is SearchCepLoading) {
                    return const Expanded(
                        child: Center(
                      child: CircularProgressIndicator(),
                    ));
                  }

                  return Text(
                      'Cidade ${(state as SearchCepSuccess).json["localidade"]}');
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _searchCepBlc.close();
    super.dispose();
  }

  void _searchCepA() async {
    final cep = _textController.text;
    _searchCepBlc.add(cep);
  }
}
